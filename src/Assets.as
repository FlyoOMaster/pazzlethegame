package 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import starling.display.Image;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	/**
	 * ...
	 * @author Yarcho
	 */
	public class Assets 
	{	
		[Embed(source = "Media/img/winner.png")]
		private static var Winner:Class;
				
		[Embed(source = "Media/img/pic_1.jpg")]
		private static var pic_1:Class;
		
		[Embed(source = "Media/img/pic_2.jpg")]
		private static var pic_2:Class;
		
		[Embed(source = "Media/img/pic_3.jpg")]
		private static var pic_3:Class;
		
		[Embed(source = "Media/img/pic_4.jpg")]
		private static var pic_4:Class;
		
		[Embed(source = "Media/img/pic_5.jpg")]
		private static var pic_5:Class;
		
		[Embed(source = "Media/img/pic_6.jpg")]
		private static var pic_6:Class;
		
		private static var textureConatainer:Object = {};
		private static var maskTextures:Dictionary;
		private static var maskTextureAtlas:TextureAtlas;
		
		[Embed(source = "Media/img/pazzles.png")]
		public static const AtlasMaskTexture:Class;
		
		[Embed(source = "Media/img/pazzles.xml", mimeType='application/octet-stream')]
		public static const AtlasMaskXML:Class;
		
		public static function getMaskAtlas():TextureAtlas
		{
			if (!maskTextureAtlas) 
			{
				var texture:Texture = getTexture('AtlasMaskTexture');
				var xml:XML = XML(new AtlasMaskXML());
				
				maskTextureAtlas = new TextureAtlas(texture, xml);
			}
			return maskTextureAtlas;
		}
				
		public static function getTexture(name:String):Texture 
		{
			if (!textureConatainer[name]) 
			{
				var texture:Texture = Texture.fromBitmap(new Assets[name]());		
				textureConatainer[name] = texture;
				trace("create texture instance " + name);
			}				
			return textureConatainer[name];
		}
		
		public static function getCroppedTexture(image:Image, x:int, y:int, width:int, height:int):Texture
		{
			var sourceTexture:Texture = image.texture;
			return Texture.fromTexture(sourceTexture, new Rectangle(x, y, width, height));
			
		}
	}

}