package logic 
{
	import GameObjects.PuzzleElement.PuzzleEvent;
	import flash.display.InteractiveObject;
	/**
	 * ...
	 * @author ...
	 */
	public class GameScoreData 
	{
		
		public function GameScoreData() 
		{
			
		}
		
		public function playTime():uint 
		{
			return time - pauseTime;
		}
		
		private var _points		: int = 0;
		public var time			: uint = 0;
		public var pauseTime	: uint = 0;
		
		public function get points():int 
		{
			return _points;
		}
		
		public function set points(value:int):void 
		{
			_points = value;
			PuzzleEvent.dispatch(PuzzleEvent.GAME_SCORE_PROGRESS, this );
		}
	}

}