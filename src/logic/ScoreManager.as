package logic 
{
	import GameObjects.PuzzleElement.PuzzleEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import starling.events.EventDispatcher;
	/**
	 * ...
	 * @author ...
	 */
	public class ScoreManager 
	{
		private static var _currScore:GameScoreData;
		private static var _timer:Timer;
		private static var _played:Boolean = false;
		
		public static function initNewGame():void
		{
			_currScore = new GameScoreData();
			PuzzleEvent.dispatcher.addEventListener(PuzzleEvent.PAZZLE_COMPLETE, onPuzzleComplete);
			_timer = new Timer(1000, 0);
			_played = false;
			_timer.addEventListener(TimerEvent.TIMER, onTimerTick);
			_timer.start();
			
			setPlayTimer(true);
		}
		
		private static function onPuzzleComplete(e:PuzzleEvent):void 
		{
			setPlayTimer(false);
			_timer.stop();
		}
		
		private static function onTimerTick(e:TimerEvent):void 
		{
			_currScore.time++;
			if (_played)
				PuzzleEvent.dispatch( PuzzleEvent.GAME_TIMER_TICK, _currScore );
			else
				_currScore.pauseTime++;
		}
		
		public static function setPlayTimer(value:Boolean):void
		{
			if (value == _played)
				return;
				
			_played = value;
			PuzzleEvent.dispatch( _played ? PuzzleEvent.GAME_TIMER_START : PuzzleEvent.GAME_TIMER_PAUSE, _currScore );
		}
		
		static public function setPuzzleElement(completeNum:int, fulLength:int):void 
		{
			_currScore.points += 100;
		}
	}

}