package logic 
{
	import GameObjects.PuzzleElement.PuzzleContainer;
	import GameObjects.ui.PauseScreen;
	import GameObjects.ui.WindowManager;
	/**
	 * ...
	 * @author ...
	 */
	public class GameSessionManager 
	{
		
		public static function startGame(pictureID:String):void
		{
			
			WindowManager.showWindow("mainWindow", null);
			
			PauseScreen.initGamePause();
			ScoreManager.initNewGame();	
			
			var _puzzleCont:PuzzleContainer = new PuzzleContainer(pictureID);
			Game.puzzleCont.addChildAt(_puzzleCont, 0);
			_puzzleCont.init();
			
		}
	}

}