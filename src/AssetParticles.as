package 
{
	/**
	 * ...
	 * @author Yarcho
	 */
	public class AssetParticles 
	{
		
		[Embed(source="Media/particles/particle.pex", mimeType="application/octet-stream")]
		public static var ParticleXML:Class;
		
		
		[Embed(source="Media/particles/texture.png")]
		public static var ParticleTextre:Class;
		
		
	}

}