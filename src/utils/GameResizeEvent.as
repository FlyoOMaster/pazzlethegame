package utils 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author ...
	 */
	public class GameResizeEvent extends Event 
	{
		public static const STAGE_RESIZED:String = "STAGE_RESIZED";
			
		public function GameResizeEvent(type:String, bubbles:Boolean=false, data:Object=null) 
		{
			super(type, bubbles, data);
			
		}
		
	}

}