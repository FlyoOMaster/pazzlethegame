package utils 
{
	import flash.geom.Point;
	import starling.display.DisplayObject;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	/**
	 * ...
	 * @author ...
	 */
	public class TouchHandler 
	{
		private static const TOLERANCE_DISTANCE : Number = 5;
		
		private var _onClickFunc:Function = null;
		private var _obj:DisplayObject;
		private var _stX:Number;
		private var _stY:Number;
		
		public function TouchHandler(obj:DisplayObject, setMouseEnable:Boolean = true ) 
		{
			_obj = obj;
			_obj.addEventListener(TouchEvent.TOUCH, onTouch );
		}
		
		public function distroy():void 
		{
			_obj.removeEventListener(TouchEvent.TOUCH, onTouch);
			_obj = null;
			_onClickFunc = null;
		}
		
		private function onTouch(e:TouchEvent):void 
		{
			var touch:Touch = e.getTouch( _obj );
			
			if (touch == null)
				return;
			
			switch(touch.phase)
			{
				case TouchPhase.BEGAN:
					_stX = touch.globalX;
					_stY = touch.globalY;
					break;
					
				case TouchPhase.ENDED:
					if (distanceToStart(touch) <= TOLERANCE_DISTANCE)
						process(_onClickFunc, e);
			}
		}
		
		private function process(func:Function, e:TouchEvent):void 
		{
			if (func != null)
				func(e);
		}
		
		public function set onClickFunc(value:Function):void 
		{
			_onClickFunc = value;
		}
		
		private function distanceToStart(touch:Touch):Number
		{
			var fX:Number = touch.globalX;
			var fY:Number = touch.globalY;
			return Math.pow((_stX - fX) * (_stX - fX) + (_stY - fY) * (_stY - fY), 0.5);
		}
		
	}

}