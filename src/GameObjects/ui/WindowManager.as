package GameObjects.ui 
{
	import flash.utils.Dictionary;
	import starling.display.DisplayObjectContainer;
	import starling.events.Event;
	import utils.GameResizeEvent;
	/**
	 * ...
	 * @author ...
	 */
	public class WindowManager 
	{
		private static var _windows:Object = 
		{
			"mainWindow"			: MainWindow,
			"pauseWindow"			: PauseScreen,
			"selectGameWindow"		: SelectGameWindow
		}
		
		
		
		private static var _winDict:Dictionary = new Dictionary();
		
		private static var _mainCont:DisplayObjectContainer;
		public static function init(mainCont:DisplayObjectContainer):void
		{
			_mainCont = mainCont;
			_mainCont.stage.addEventListener(GameResizeEvent.STAGE_RESIZED, onStageResize);
		}
		
		static private function onStageResize(e:Event):void 
		{
			for (var i:int = 0; i < _mainCont.numChildren; i++ )
				(_mainCont.getChildAt(i) as WindowBase).layout(_mainCont.stage.stageWidth, _mainCont.stage.stageHeight);
		}
		
		public static function showWindow( winName:String, args:* = null):void
		{
			var cWindow:WindowBase ;
			if (!_winDict.hasOwnProperty(winName))
			{
				var cClass:Class = _windows[winName];
				cWindow = new cClass();
				_winDict[winName] = cWindow;
			}
			
			cWindow = _winDict[winName];
			cWindow.initWithData(args);
			_mainCont.addChild(cWindow );
			cWindow.show();
			cWindow.layout(_mainCont.stage.stageWidth, _mainCont.stage.stageHeight);
		}
		
		static public function hideWindow(winName:String):void 
		{
			var win:WindowBase = _winDict[winName];
			if (win && win.stage)
				win.removeFromParent();
		}
	}

}