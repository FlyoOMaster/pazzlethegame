package GameObjects.ui 
{
	/**
	 * ...
	 * @author ...
	 */
	public class WinnerWindow extends WindowBase 
	{
		
		private var _winner:Image;
		
		public function WinnerWindow() 
		{
			super();
			_winner = new Image(Assets.getTexture('Winner'));
			_winner.x = stage.stageWidth >> 1;
			_winner.y = stage.stageHeight >> 1;
			_winner.visible = false;
			_winner.pivotX = _winner.width >> 1;
			_winner.pivotY = _winner.height >> 1;
			this.addChild(_winner);
			
		}
		
		override public function show():void
		{
			TweenMax.to(_winner, 0.5, {
				ease:Expo.easeOut,
				scale:0.5, 
				repeatDelay:0.1, 
				repeat:3, 
				yoyo:true
			});
		}
		
	}

}