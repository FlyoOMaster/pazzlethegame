package GameObjects.ui 
{
	import GameObjects.PuzzleElement.PuzzleEvent;
	import GameObjects.visual.UIButton;
	import logic.ScoreManager;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.utils.Color;
	/**
	 * ...
	 * @author ...
	 */
	public class MainWindow extends WindowBase 
	{
		private var _showImageBtn:UIButton;
		private var _timer:TextField;
		private var _score:TextField;
		private var _pauseBtn:UIButton;
		
		public function MainWindow() 
		{
			super();

			_showImageBtn = new UIButton();
			_showImageBtn.addEventListener(TouchEvent.TOUCH, onShowImageTouch);
			this.addChild(_showImageBtn);
			
			_pauseBtn = new UIButton( "Pause" );
			addChild( _pauseBtn );
			_pauseBtn.y = 128;
			_pauseBtn.touch.onClickFunc = onPauseTouch;
			
			var tf:TextFormat = new TextFormat( "Verdana", 20, Color.WHITE );
			_timer = new TextField( 150, 40, "0", tf);
			addChild(_timer);
			_timer.x = 400;
			PuzzleEvent.dispatcher.addEventListener(PuzzleEvent.GAME_TIMER_TICK, onPuzzleTick);
			
			_score = new TextField( 150, 40, "0", tf);
			addChild(_score);
			_score.x = 600;
			PuzzleEvent.dispatcher.addEventListener(PuzzleEvent.GAME_SCORE_PROGRESS, onGameProgress);		
		}
		
		private function onPuzzleTick(e:PuzzleEvent):void 
		{
			_timer.text = String(e.score.playTime());
		}
		
		private function onGameProgress(e:PuzzleEvent):void 
		{
			_score.text = String(e.score.points);
		}
		
		
		private function onShowImageTouch(e:TouchEvent):void 
		{
			if(e.touches[0].phase == TouchPhase.BEGAN)
				PuzzleEvent.dispatch(PuzzleEvent.VIS_MODE_HIDE_PUZZLE_ELTS, null );
		}
		
		private function onPauseTouch(e:TouchEvent):void 
		{
			ScoreManager.setPlayTimer(false);
		}
		
	}

}