package GameObjects.ui 
{
	import GameObjects.visual.UIButton;
	import com.greensock.TweenMax;
	import logic.GameSessionManager;
	import starling.animation.Tween;
	import starling.display.Image;
	import starling.events.TouchEvent;
	import utils.TouchHandler;
	/**
	 * ...
	 * @author ...
	 */
	public class SelectGameWindow extends WindowBase 
	{
		
		private var _currPic:Image;
		private var _currPicTouch:TouchHandler;
		private var _currPicIndex:int;
		
		private var _startBtn:UIButton;
		
		public function SelectGameWindow() 
		{
			super();
			
			_pics = [];
			
			for (var i:int = 0; i < 6; i++ )
			{
				var pic:Image = new Image(Assets.getTexture("pic_" + (i + 1)));
				addChild(pic);
				pic.width = 192;
				pic.height = 96;
				
				pic.x = i % 3 * 200;
				pic.y = i % 2 * 100;
				_pics.push( pic );
				var h:TouchHandler = new TouchHandler( pic );
				h.onClickFunc = onImgClick;
			}
			
			_startBtn = new UIButton( "start" );
			_startBtn.x = (600 - _startBtn.width) * 0.5;
			_startBtn.y = (200 - _startBtn.height) * 0.75;
			
			new TouchHandler( _startBtn ).onClickFunc = onStartClick;
		}
		
		private function onStartClick(e:TouchEvent):void 
		{
			WindowManager.hideWindow("selectGameWindow");
			GameSessionManager.startGame("pic_" + (_currPicIndex + 1));
			onPreviewClick(e);
		}
		
		private function onImgClick(e:TouchEvent):void 
		{
			var picIndex:int = _pics.indexOf(e.currentTarget);
			
			var pic:Image = new Image(Assets.getTexture("pic_" + (picIndex + 1)));
			pic.width = 192;
			pic.height = 96;
			
			pic.x = picIndex % 3 * 200;
			pic.y = picIndex % 2 * 100;
			addChild(pic);
			
			_currPic = pic;
			_currPicIndex = picIndex;
			
			this.touchable = false;
			new TweenMax( pic, 1, {x: 0, y:0, width:600, height:200, onComplete:onTweenComplete } ).play();
		}
		
		private function onTweenComplete():void 
		{
			this.touchable = true;
			
			_currPicTouch = new TouchHandler(_currPic );
			_currPicTouch.onClickFunc = onPreviewClick;
			addChild(_startBtn);
		}
		
		private function onPreviewClick(e:TouchEvent):void 
		{
			removeChild(_currPic);
			_currPicTouch.distroy();
			_currPicTouch = null;
			_currPic = null;
			removeChild(_startBtn);
		}
		
		private function onCurrPickClick(e:TouchEvent):void
		{
			
		}
		
		override public function layout(w:Number, h:Number):void
		{
			this.x = (w - 600) * 0.5; 
			this.y = (h - 200) * 0.5; 
		}
		
		private var _pics:Array;
	}

}