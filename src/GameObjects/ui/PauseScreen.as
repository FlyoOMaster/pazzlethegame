package GameObjects.ui 
{
	import GameObjects.PuzzleElement.PuzzleEvent;
	import logic.ScoreManager;
	import starling.display.DisplayObjectContainer;
	import starling.display.Quad;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.utils.Color;
	import utils.TouchHandler;
	
	/**
	 * ...
	 * @author ...
	 */
	public class PauseScreen extends WindowBase 
	{
		public static function initGamePause():void
		{
			PuzzleEvent.dispatcher.addEventListener(PuzzleEvent.GAME_TIMER_START, onGameStart);
			PuzzleEvent.dispatcher.addEventListener(PuzzleEvent.GAME_TIMER_PAUSE, onGamePause);
		}
		private static function onGameStart(e:PuzzleEvent):void 
		{
			WindowManager.hideWindow("pauseWindow");
		}
		
		private static function onGamePause(e:PuzzleEvent):void 
		{
			WindowManager.showWindow("pauseWindow");
		}
		
		private var _touchHandler:TouchHandler;
		public function PauseScreen() 
		{
			super();
			_touchHandler = new TouchHandler(this);
			_touchHandler.onClickFunc = onClick;
			addChild(new Quad(1366, 768, 0xFF000000));
			var tf:TextFormat = new TextFormat( "Verdana", 20, Color.WHITE );				
			var textField:TextField = new TextField( 150, 40, "0", tf);
			addChild(textField);
			textField.x = (this.width - textField.width) * 0.5;
			textField.y = (this.height - textField.height) * 0.5;
			
			textField.text = "PAUSE";
			
			
		}
		
		private function onClick(e:TouchEvent):void 
		{
			ScoreManager.setPlayTimer(true);
		}
		
		
		
	}

}