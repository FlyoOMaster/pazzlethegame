package GameObjects.logic 
{
	/**
	 * ...
	 * @author ...
	 */
	public class GameSessionProcessor 
	{
		private static var _instance:GameSessionProcessor;
		public function get instance():GameSessionProcessor
		{
			return _instance;
		}
		
		public static function init():void
		{
			_instance = new GameSessionProcessor();
		}
		
		public function GameSessionProcessor() 
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			_puzzleContainer = new PazzleContainer();
			PuzzleEvent.dispatcher.addEventListener(PuzzleEvent.PAZZLE_COMPLETE, onPazzleComplete);
			this.addChild(_puzzleContainer);	
			_puzzleContainer.init();
		}
		
		private function onKeyDown(e:KeyboardEvent):void 
		{
			switch (true) 
			{
				case e.keyCode == Keyboard.W :
					win();
				break;
				default:
			}
		}
		
		private function onKeyUp(e:KeyboardEvent):void 
		{
			switch (true) 
			{
				case e.keyCode == Keyboard.SPACE:
					PuzzleEvent.dispatch(PuzzleEvent.VIS_MODE_HIDE_PUZZLE_ELTS, null );
				break;
				default:
			}
		}
		
		private function onPazzleComplete(e:Event):void 
		{
			WindowManager.showWindow("winnerWindow");
		}
		
		private function win():void
		{
			PuzzleEvent.dispatch(PuzzleEvent.PAZZLE_COMPLETE, null );
		}
		
	}

}