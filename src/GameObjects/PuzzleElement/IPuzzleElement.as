package GameObjects.PuzzleElement 
{
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Yarcho
	 */
	public interface IPuzzleElement 
	{
		function get targetPosition():Point;
		
		function get isSeted():Boolean;
		
		function get targetX():int;
		function set targetX(val:int):void;
		
		function get targetY():int;
		function set targetY(val:int):void;
		
		function get distanceToTarget():Point;
		
		function get isDrag():Boolean;
		function set isDrag(value:Boolean):void;
	}
	
}