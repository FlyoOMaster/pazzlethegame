package GameObjects.PuzzleElement 
{
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import logic.ScoreManager;
	import starling.core.Starling;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.utils.deg2rad;
	
	import starling.textures.Texture;
	import starling.display.Image;
	import starling.display.Sprite;
	
	/**
	 * ...
	 * @author Yarcho
	 */
	public class PuzzleContainer extends Sprite implements IPuzzleConatiner 
	{
		private var _image:Image;
		private var _puzzles:Vector.<BasePuzzleElement>;
		private var _count:int;
		private var _bitmap:Bitmap;
		private var _mousePos:Point;
		private var _selectedPazzle:BasePuzzleElement;
		private var _bg:Image;
		private var _particle:PDParticleSystem;
		
		private var _completePuzzlesNum:int = 0;
		
		private var _visMode:String;
		private var _picID:String;
		
		
		public function get pazzles():Vector.<BasePuzzleElement> 
		{
			return _puzzles;
		}
		
				
		public function get count():int 
		{
			return _count;
		}
		
		public function set count(value:int):void 
		{
			_count = value;
		}
		
		public function get image():Image 
		{
			return _image;
		}
		
		public function set image(img:Image):void
		{
			
			if (_bg) this.removeChild(_bg);
			_bg = img;
			_bg.alpha = 0.3;
			this.addChild(_bg);
			
			if (_image) this.removeChild(_image);
			_image = img;			
			this.addChild(_image);
			
			if (count < 2) return;
			splitImage();
		}
		
		private function swithVisMode(e:PuzzleEvent):void 
		{
			showImageMode( _visMode == PuzzleEvent.VIS_MODE_SHOW_PUZZLE );
		}

		public function PuzzleContainer(picID:String) 
		{
			super();
			_picID = picID;
			_puzzles = new Vector.<BasePuzzleElement>();		
			_visMode = PuzzleEvent.VIS_MODE_SHOW_PUZZLE;
			this.addEventListener(Event.ADDED_TO_STAGE, onAddToStage);
			PuzzleEvent.dispatcher.addEventListener(PuzzleEvent.VIS_MODE_HIDE_PUZZLE_ELTS, swithVisMode);
		}
		
		public function init():void
		{
			this.count = 6;//trust me, i'm engineer
			this.image = new Image(Assets.getTexture(_picID));
			this.x = (stage.stageWidth - this.width) >> 1;
			this.y = (stage.stageHeight - this.height) >> 1;
			this.shuffle();
			
		}
		
		public function showImageMode(mode:Boolean):void
		{
			_visMode = mode ? PuzzleEvent.VIS_MODE_SHOW_IMAGE : PuzzleEvent.VIS_MODE_SHOW_PUZZLE;
			
			var puzzlesCnt:int = _puzzles.length;
			for (var i:int = 0; i < puzzlesCnt; i++) 
				_puzzles[i].visible = mode ? _puzzles[i].isSeted : true;
				
			PuzzleEvent.dispatch( _visMode, this );
		}
		
		//Разбиваем изображение на пазлы
		public function splitImage():Vector.<BasePuzzleElement> 
		{
			clearPazzles();
			
			var horizCount:int = image.width / BasePuzzleElement.SIZE,
				verticCount:int = image.height / BasePuzzleElement.SIZE;
				
			for (var i:int = 0; i < horizCount; i++) 
			{
				for (var j:int = 0; j < verticCount; j++) 
				{
					var mask:Image = getMask(i, j, horizCount-1, verticCount-1),
						x:int = i*mask.width,
						y:int = j * mask.height,
						texture:Image = new Image(Assets.getCroppedTexture(image, x, y, mask.width, mask.height));
						
						
					var pazzle:BasePuzzleElement = new BasePuzzleElement(texture, mask, new Point(x - i*13, y-j*14));
					pazzle.x = x*1.05;
					pazzle.y = y * 1.05;
					pazzle.addEventListener(PuzzleEvent.START_DRAG, onStartDrag);
					pazzle.addEventListener(PuzzleEvent.STOP_DRAG, onStopDrag);
					_puzzles.push(pazzle);	
					this.addChild(pazzle);	
				}
			}
			
			_bg.width = _image.width - horizCount * 13;
			_bg.height = _image.height - verticCount * 13;
			return _puzzles;
		}
		
		private function getMask(i:int, j:int, maxX:int, maxY:int):Image 
		{
			var img:Image;
			//top
			if (j == 0) 
			{
				if (i == 0) 
				{
					return new Image(Assets.getMaskAtlas().getTexture('L_T'));
				} 
				else if (i == maxX) 
				{
					return new Image(Assets.getMaskAtlas().getTexture('R_T'));
				}
				
				return i % 2 != 0 
					? new Image(Assets.getMaskAtlas().getTexture('T_EVEN')) 
					:  new Image(Assets.getMaskAtlas().getTexture('T_ODD'));
			}
			//middle
			if (j > 0 && j < maxY) 
			{
				//start
				if (i == 0) 
				{
					return j % 2 == 0 
					? new Image(Assets.getMaskAtlas().getTexture('L_EVEN')) 
					:  new Image(Assets.getMaskAtlas().getTexture('L_ODD'));
				} 
				//end
				else if (i == maxX) 
				{
					return j % 2 != 0 
					? new Image(Assets.getMaskAtlas().getTexture('R_EVEN')) 
					:  new Image(Assets.getMaskAtlas().getTexture('R_ODD'));
				}
				//if even row
				if (j % 2 == 0) 
				{
					return i % 2 == 0 
						? new Image(Assets.getMaskAtlas().getTexture('EVEN')) 
						:  new Image(Assets.getMaskAtlas().getTexture('ODD'));
				} 
				else 
				{
					return i % 2 != 0 
						? new Image(Assets.getMaskAtlas().getTexture('EVEN')) 
						:  new Image(Assets.getMaskAtlas().getTexture('ODD'));
				}
				
				
			}
			//bottom
			if (j == maxY) 
			{
				if (i == 0) 
				{
					return new Image(Assets.getMaskAtlas().getTexture('L_B'));
				} 
				else if (i == maxX) 
				{
					return new Image(Assets.getMaskAtlas().getTexture('R_B'));
				}
				
				return i % 2 != 0 
					? new Image(Assets.getMaskAtlas().getTexture('B_EVEN')) 
					:  new Image(Assets.getMaskAtlas().getTexture('B_ODD'));
			}
			return null;
		}

		//Провреяем, закончен ли пазл
		private function checkPazzleComplete( elt:BasePuzzleElement ):void 
		{
			if (elt.isSeted)
			{
				_completePuzzlesNum++;
				elt.setComplete();
				setChildIndex(elt, 0 );
				elt.removeEventListener(PuzzleEvent.START_DRAG, onStartDrag);
				elt.removeEventListener(PuzzleEvent.STOP_DRAG, onStopDrag);
				ScoreManager.setPuzzleElement(_completePuzzlesNum, _puzzles.length);
				if (_completePuzzlesNum == _puzzles.length)
					PuzzleEvent.dispatch(PuzzleEvent.PAZZLE_COMPLETE, null);
			}
		}
		
		//Очистить место установки от пазлов
		private function clearPazzleArea():void 
		{
			if (!_selectedPazzle) return;
			
			var pazzlesCount:int = _puzzles.length;
			for (var i:int = 0; i < pazzlesCount; i++) 
			{
				var pazzle:BasePuzzleElement = _puzzles[i];
				var selectedPazzleBound:Rectangle = _selectedPazzle.getBounds(this);
				var pazzleBound:Rectangle = pazzle.getBounds(this);
				if (!pazzle.isSeted && pazzle != _selectedPazzle && selectedPazzleBound.intersects(pazzleBound))
				{
					var newX:int = calcXPosition(pazzle);
					var newY:int = calcYPosition(pazzle);
					
					TweenLite.to(pazzle, 0.5, {
						x: newX,
						y: newY
					});
					this.setChildIndex(pazzle, this.numChildren);
				}
			}
		}
		
		//Расчитать новую координату Y пазла для в момент использования clearPazzleArea
		private function calcYPosition(pazzle:BasePuzzleElement):int 
		{
			if ((pazzle.y - pazzle.height) < 0) 
			{				
				return pazzle.y + pazzle.height * 2;
			} else 
			{
				return pazzle.y - pazzle.height;
			}
		}
		
		//Расчитать новую координату X пазла для в момент использования clearPazzleArea
		private function calcXPosition(pazzle:BasePuzzleElement):int 
		{
			if ((pazzle.x - pazzle.width) < 0) 
			{				
				return pazzle.x + pazzle.width * 2;
			} else 
			{
				return pazzle.x - pazzle.width;
			}
		}
		
		//Установить пазл в его расположение
		private function setPazzleToTarget(pazzle:BasePuzzleElement, onSetted:Function):void 
		{
			TweenLite.to(pazzle, 0.2, {
					x:_selectedPazzle.targetPosition.x,
					y:_selectedPazzle.targetPosition.y,
					onComplete:onSetted,
					onCompleteParams:[pazzle]
				});
		}
		
		//Очистить игровое поле
		private function clearPazzles():void 
		{
			for (var i:int = 0; i < _puzzles.length; i++) 
			{
				var pazzle:BasePuzzleElement = _puzzles[i];
				pazzle.removeEventListener(PuzzleEvent.START_DRAG, onStartDrag);
				pazzle.removeEventListener(PuzzleEvent.STOP_DRAG, onStopDrag);
				
				this.removeChild(pazzle);				
				_puzzles.splice(i, 1);
			}
		}
		
		//Перемешать пазлы
		public function shuffle():void 
		{
			var pazzle:BasePuzzleElement;
			for (var i:int = 0; i < _puzzles.length; i++) 
			{
				pazzle = _puzzles[i];
				pazzle.x = Math.random() * image.width;
				pazzle.y = Math.random() * image.height;
			}
		}
		
		//Проверить установку пазла
		public function checkPosition():Boolean 
		{
			return _selectedPazzle.distanceToTarget.x <= 25 && _selectedPazzle.distanceToTarget.y <= 25;
		}
		
		private function onAddToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddToStage);
			
			if (image && _puzzles.length == 0 && count > 1) 
			{
				splitImage();
			}
			
			_particle = new PDParticleSystem(XML(new AssetParticles.ParticleXML()), Texture.fromBitmap(new AssetParticles.ParticleTextre()));
			Starling.juggler.add(_particle);
			this.addChild(_particle);
			
			this.addEventListener(TouchEvent.TOUCH, onMouseMove);
		}
		
		private function onMouseMove(e:TouchEvent):void 
		{
			var touch:Touch = e.getTouch(this);
			if (touch && _selectedPazzle && (
				touch.phase == TouchPhase.BEGAN || 
				touch.phase == TouchPhase.HOVER || 
				touch.phase == TouchPhase.MOVED)
				) 
			{
				var pos:Point = touch.getLocation(this);
				this.setChildIndex(_selectedPazzle, this.numChildren);
				TweenLite.to(_selectedPazzle, 0.2, {
					x:pos.x - (_selectedPazzle.width >> 1),
					y:pos.y - (_selectedPazzle.height >> 1)
				});
			}					
		}
		
		private function onStartDrag(e:PuzzleEvent):void 
		{
			_selectedPazzle = e.puzzle;
			_selectedPazzle.isDrag = true;
			setChildIndex(_selectedPazzle, numChildren);
		}
		
		private function onStopDrag(e:PuzzleEvent):void 
		{
			if (checkPosition()) 
			{
				setPazzleToTarget(_selectedPazzle, onPazzleSet);
				clearPazzleArea();	
			}
			_selectedPazzle.isDrag = false;
			_selectedPazzle = null;
		}
				
		private function onPazzleSet(pazzle:BasePuzzleElement):void 
		{
			_particle.x = pazzle.x + (pazzle.width >> 1);
			_particle.y = pazzle.y + (pazzle.height >> 1);
			_particle.start(0.2);
			
			checkPazzleComplete( pazzle );
		}
	}

}