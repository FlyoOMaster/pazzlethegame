package GameObjects.PuzzleElement 
{
	import logic.GameScoreData;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	/**
	 * ...
	 * @author ...
	 */
	public class PuzzleEvent extends Event 
	{
		public static const START_DRAG					: String = 'startDrag';
		public static const STOP_DRAG					: String = 'stopDrag';
		
		public static const PAZZLE_COMPLETE				: String = 'pazzleComplete';
		
		public static const VIS_MODE_SHOW_IMAGE			: String = "VIS_MODE_SHOW_IMAGE";
		public static const VIS_MODE_SHOW_PUZZLE		: String = "VIS_MODE_SHOW_PUZZLE";
		public static const VIS_MODE_HIDE_PUZZLE_ELTS	: String = "VIS_MODE_HIDE_PUZZLE_ELTS";
		
		public static const GAME_TIMER_TICK				: String = "GAME_TIMER_TICK"
		public static const GAME_TIMER_START			: String = "GAME_TIMER_START";
		public static const GAME_TIMER_PAUSE			: String = "GAME_TIMER_PAUSE";
		
		public static const GAME_SCORE_PROGRESS			: String = "GAME_SCORE_PROGRESS";
		
		private static var _dispatcher:EventDispatcher;
		
		public static function get dispatcher():EventDispatcher
		{
			if (_dispatcher == null)
				_dispatcher = new EventDispatcher();
				
			return _dispatcher;
		}
		
		public static function dispatch( type:String, someData:*):void
		{
			dispatcher.dispatchEvent( new PuzzleEvent(type, someData ) );
		}
		
		public function PuzzleEvent(type:String, obj:*=null) 
		{
			super(type, false, obj);
		}
		
		public function get puzzle():BasePuzzleElement
		{
			return data as BasePuzzleElement;
		}
		
		public function get score():GameScoreData
		{
			return data as GameScoreData;
		}
	}

}