package GameObjects.PuzzleElement 
{
	import com.greensock.TweenLite;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.TextureMaskStyle;
	import starling.styles.MeshStyle;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author Yarcho
	 */
	public class BasePuzzleElement extends Sprite implements IPuzzleElement 
	{
		public static const SIZE:int = 128;
		
		private var _targetPosition:Point;
		private var _image:Image;
		private var _mask:Image;
		private var _isDrag:Boolean;		
		
		private var _style:TextureMaskStyle;
		
		public function BasePuzzleElement(texture:Image, maskTexture:Image, targetPos:Point) 
		{
			_targetPosition = targetPos;
			_image = texture;
			_mask = maskTexture;
			
			
			if (_style == null)
				_style = new TextureMaskStyle();
			_mask.style = _style;
			this.mask = _mask;
			
			this.addChild(_image);	
			this.addEventListener(TouchEvent.TOUCH, onTouchThis );
		}
		
		private function onTouchThis(e:TouchEvent):void 
		{
			var ph:String = e.touches[0].phase;
			
			switch(ph)
			{
				case TouchPhase.BEGAN:
					dispatch( PuzzleEvent.START_DRAG );
					break;
				
				case TouchPhase.ENDED:
					dispatch( PuzzleEvent.STOP_DRAG );
					break;
			}
		}
		
		private function dispatch(eType:String):void
		{
			dispatchEvent(new PuzzleEvent( eType, this ) );
		}
		
		public function setComplete():void 
		{
			touchable = false;
			this.removeEventListener(TouchEvent.TOUCH, onTouchThis );
		}
		
		public function get isSeted():Boolean 
		{
			return targetX == x && targetY == y;
		}
		
		public function get targetX():int 
		{
			return targetPosition.x;
		}
		
		public function set targetX(val:int):void
		{
			targetPosition.x = val;			
		}
		
		public function get targetY():int 
		{
			return targetPosition.y;
		}
		
		public function set targetY(val:int):void
		{
			targetPosition.y = val;
		}
		
		
		public function get isDrag():Boolean 
		{
			return _isDrag;
		}
		
		public function set isDrag(value:Boolean):void
		{
			_isDrag = value;
		}
		
		public function get targetPosition():Point 
		{
			return _targetPosition;
		}
		
		public function get distanceToTarget():Point
		{
			var abs:Function = Math.abs;
			return new Point(abs(x - targetPosition.x), abs(y - targetPosition.y));
		}
	}
}