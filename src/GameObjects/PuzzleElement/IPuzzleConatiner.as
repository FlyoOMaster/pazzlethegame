package GameObjects.PuzzleElement 
{
	import flash.display.Bitmap;
	import starling.display.Image;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Yarcho
	 */
	public interface IPuzzleConatiner 
	{
		function get count():int;
		function set count(val:int):void;
		
		function get image():Image;
		function set image(img:Image):void;
		
		function get pazzles():Vector.<BasePuzzleElement>;
			
		function shuffle():void;
		
		function checkPosition():Boolean;
	}
	
}