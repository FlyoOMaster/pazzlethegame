package GameObjects.visual 
{
	import starling.display.Sprite;
	import starling.display.Quad;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.utils.Color;
	import utils.TouchHandler;
	
	/**
	 * ...
	 * @author ...
	 */
	public class UIButton extends Sprite 
	{
		private var _touch:TouchHandler;
		
		
		public function UIButton(text:String = null) 
		{
			addChild(new Quad(128, 64, 0xFF000000 + 0xFFFFFF * Math.random()));
			if (text != null)
			{
				var tf:TextFormat = new TextFormat( "Verdana", 20, Color.BLACK );
				var textField:TextField = new TextField( 150, 40, "0", tf);
				addChild(textField);
				textField.text = text;
			}
		}
		
		public function get touch():TouchHandler 
		{
			if (_touch == null)
				_touch = new TouchHandler(this);
			return _touch;
		}
		
	}

}