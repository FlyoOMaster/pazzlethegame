package 
{
	import GameObjects.PuzzleElement.BasePuzzleElement;
	import GameObjects.PuzzleElement.IPuzzleConatiner;
	import GameObjects.PuzzleElement.PuzzleContainer;
	import GameObjects.PuzzleElement.PuzzleEvent;
	import GameObjects.ui.WindowManager;
	import GameObjects.ui.PauseScreen;
	import GameObjects.visual.UIButton;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Expo;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import logic.GameSessionManager;
	import logic.ScoreManager;
	import starling.core.Starling;
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import starling.textures.Texture;
	import starling.utils.Color;
	
	/**
	 * ...
	 * @author Yarcho
	 */
	public class Game extends Sprite 
	{
		public static var puzzleCont:DisplayObjectContainer;
		private static var _winCont:DisplayObjectContainer;
		
		public function Game() 
		{
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddToStage);
		}
		
		private function onAddToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddToStage);
			
			puzzleCont = new Sprite();
			addChild(puzzleCont);
			
			_winCont = new Sprite();
			addChild(_winCont);
			WindowManager.init(_winCont);
			
			WindowManager.showWindow("selectGameWindow");			
		}
	
		
	}

}