package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import starling.core.Starling;
	import utils.GameResizeEvent;
	/**
	 * ...
	 * @author Yarcho
	 */
	public class Main extends Sprite 
	{
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.addEventListener(Event.RESIZE, onMainStageResize);
			new Starling(Game, stage).start();
			Starling.current.showStats = true;
		}
		
		private function onMainStageResize(e:Event):void 
		{
			Starling.current.viewPort.width = Starling.current.stage.stageWidth = this.stage.stageWidth;
			Starling.current.viewPort.height = Starling.current.stage.stageHeight = this.stage.stageHeight;
			
			Starling.current.stage.dispatchEvent(new GameResizeEvent(GameResizeEvent.STAGE_RESIZED));
		}
		
	}
	
}